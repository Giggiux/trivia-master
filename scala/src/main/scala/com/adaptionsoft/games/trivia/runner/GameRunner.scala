package com.adaptionsoft.games.trivia.runner

import com.adaptionsoft.games.uglytrivia.{Game, PlayerManager}
import java.util.Random


object GameRunner {
  var didSomeoneWin = false

  def main(args: Array[String]) {
    val playerManager = new PlayerManager
    playerManager.addPlayer("Chet")
    playerManager.addPlayer("Pat")
    playerManager.addPlayer("Sue")

    val aGame = new Game(playerManager)


    val rand: Random = new Random

    do {
      aGame.nextPlayerRolled(rand.nextInt(5) + 1)
      if (rand.nextInt(9) == 7) {
        aGame.wasWornglyAnswered
      }
      else {
        aGame.wasCorrectlyAnswered
      }
      didSomeoneWin = aGame.isGameOver
    } while (!didSomeoneWin)
  }
}