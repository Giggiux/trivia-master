package com.adaptionsoft.games.uglytrivia

class Player(val name: String) {
	var place = 0
	var goldCoins = 0
	private var inPenaltyBox = false

	def move(roll: Int): Unit = {
		place += roll
		if (place > 11) place -= 12
		println(s"$name's new location is $place")
	}

	def winAGoldCoin(): Unit = {
		goldCoins += 1
		println(s"$name now has $goldCoins Gold Coins.")
	}

	def goToPenaltyBox(): Unit = {
		inPenaltyBox = true
		println(s"$name was sent to the penalty box")
	}

	def exitFromPenaltyBox(): Unit = {
		inPenaltyBox = false
		println(s"$name is getting out of the penalty box")
	}

	def isInPenaltyBox(): Boolean = inPenaltyBox

	def isWinner(): Boolean = goldCoins == 6
}
