package com.adaptionsoft.games.uglytrivia


class Game(playerManager: PlayerManager, categories: List[String] = List("Pop", "Science", "Sports", "Rock")) {

	if (!playerManager.hasEnoughPlayers()) throw new Exception("Game doesn't have enough players!")

	private val questionManager: QuestionManager = new QuestionManager(categories)

	def currentPlayer(): Player = playerManager.currentPlayer


	def nextPlayerRolled(roll: Int): Unit = {
		playerManager.nextPlayer()

		println(s"They have rolled a $roll")

		val rollIsOdd = roll % 2 != 0
		val isInPenaltyBox = currentPlayer.isInPenaltyBox
		if (!isInPenaltyBox || rollIsOdd) {
			if (rollIsOdd && isInPenaltyBox) currentPlayer.exitFromPenaltyBox

			currentPlayer.move(roll)

			questionManager.askQuestion(currentPlayer.place)
		}
		else {
			println(s"${currentPlayer.name} is not getting out of the penalty box")
		}

	}

	def wasCorrectlyAnswered: Unit = {
		if (!currentPlayer.isInPenaltyBox) {
			println("Answer was correct!!!!")
			currentPlayer.winAGoldCoin
		}
	}

	def wasWornglyAnswered: Unit = {
		println("Question was incorrectly answered")
		currentPlayer.goToPenaltyBox
	}

	def isGameOver: Boolean = {
		currentPlayer.isWinner()
	}

}