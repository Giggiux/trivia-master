package com.adaptionsoft.games.uglytrivia


class PlayerManager {
	private var players: List[Player] = Nil

	var currentPlayer: Player = null

	def addPlayer(playerName: String): Unit = {
		val player = new Player(playerName)
		players = players :+ player

		println(s"$playerName was added")
		println(s"They are player number ${players.size}")
	}

	def nextPlayer(): Unit = {
		if (currentPlayer == null) {currentPlayer = players.head}
		else {
			var nextPlayer = players.indexOf(currentPlayer) + 1

			if (nextPlayer == players.size) nextPlayer = 0
			currentPlayer = players(nextPlayer)
		}
		println(s"${currentPlayer.name} is the current player")

	}

	def hasEnoughPlayers(): Boolean = players.size >= 2
}
