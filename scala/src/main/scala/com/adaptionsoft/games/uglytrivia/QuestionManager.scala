package com.adaptionsoft.games.uglytrivia

import java.util
import java.util.LinkedList

class Category(val category: String) {
	var questions: LinkedList[String] = new util.LinkedList[String]

	def askAndRemoveQuestion(): Unit = {
		println(s"The category is $category")
		println(questions.removeFirst)
	}

	def generateQuestion(index: Int): Unit = {
		questions.addLast(s"$category Question $index")
	}
}

class QuestionManager(categoryNames: List[String]) {
	private var categories: List[Category] = Nil


	categoryNames.foreach((categoryName)=> {
		val category = new Category(categoryName)
		categories = categories :+ category
		for (i <- 1 to 50)
			category.generateQuestion(i)

	})


	def askQuestion(playerPlace: Int): Unit = {
		getCurrentCategory(playerPlace).askAndRemoveQuestion
	}

	def getCurrentCategory(playerPlace: Int): Category = {
		categories(playerPlace % categories.size)
	}

}
